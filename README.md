# Flip Theme - Style readme
Reusable theme that can be easily customised throughout CSS variables.
Three different themes have been implemented so far:
1. Ballina
2. Bounce
3. GLS ( Good Life Suites website was used as a reference)

# Websites build using this theme
## Ballina Theme
- Ballina: https://ballinabeachvillage.fastrackdev.com/ WIP 

## Bounce Theme
- Bounce: https://bouncehostels.com/
- Country Apartments Dubbo: https://countryapartmentsdubbo.fastrackdev.com WIP

## GLS Theme
- Oasis at Palm Cove : https://www.oasis-palmcove.com.au/
- Waterfront Melbourne Apartments: https://waterfrontmelbourne.com.au/

## Install
- Recommended Node: v10.15.3 
- To install dependencies, run 'npm install'
- To build a specific theme for development or production, refer to the automated tasks below

## Automated tasks

This project uses [Gulp](http://gulpjs.com) to run automated tasks for development and production builds.
The tasks are as follows with the option to add a --production flag:

gulp --website="[website name]"

configuration options for each theme can be found in the config.js folder, with the json object website. An example being:

"dubbo": {
    "theme": 'bounce',
    "brand": 'dubbo'
}

Where Bounce is the theme (main style entry point) and the brand variables are in the dubbo-theme-config.

## Technologies used

JavaScript
- [Node](https://nodejs.org/)

Styles
- [Sass](http://sass-lang.com/) via ([node-sass](https://github.com/sass/node-sass))

Optimization
- [Uglify](https://github.com/mishoo/UglifyJS)

Automation
- [Gulp](http://gulpjs.com)

Code Management
- [Git](https://git-scm.com/)

