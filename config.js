const website = {
    "ballina": {
        "theme": 'ballina',
        "brand": 'ballina'
    },
    "dubbo": {
        "theme": 'bounce',
        "brand": 'dubbo'
    },
    "flip": {
        "theme": 'bounce',
        "brand": 'dubbo'
    },
    "oasis": {
        "theme": 'gls',
        "brand": 'oasis'
    },
    "waterfront": {
        "theme": 'gls',
        "brand": 'waterfront'
    },
    "stanley": {
        "theme": "gls",
        "brand": 'stanley'
    },
    "riverstone": {
        "theme": "gls",
        "brand": 'riverstone'
    },
	"batmans": {
		"theme": "gls",
		"brand": "batmans",
	},
    "wildes": {
        "theme": "gls",
        "brand": "wildes"
    },
    "taylors-motel": {
        "theme": "gls",
        "brand": "taylors-motel"
    }
}

module.exports = {
    website,
}