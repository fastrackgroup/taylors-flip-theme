var gulp = require('gulp');
var sass = require('gulp-sass');
var sassGlob = require('gulp-sass-glob');
var notify = require('gulp-notify');
// var uglify = require('gulp-uglify');
var sourcemaps = require("gulp-sourcemaps");
var autoprefixer = require('gulp-autoprefixer');
var rev = require('gulp-rev');
var revDel = require('rev-del');
var runSequence = require('run-sequence');
var gutil = require('gulp-util');
var browserSync = require('browser-sync').create();
const argv = require('yargs').argv;
const config = require('./config');

const currentTheme = config.website[argv.website].theme;
const currentBrand = config.website[argv.website].brand;

var themePath = 'scss/themes/'+ currentTheme;
var currentThemeConfigPath = themePath + '/' + currentBrand + '-theme-config';

gulp.task('default', function() {
    if (argv.production) {
        gulp.start('production');
    } else {
        gulp.start('dev');
    }
    return;
});

gulp.task('browserSync', function() {
    browserSync.init({
        proxy: "https://" + currentBrand + ".local",
        https: true,
        ui: false,
    });
});

function versionFile(source, destination, fileName) {
	return gulp.src(source)
		.pipe(sassGlob())
		.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
		.pipe(autoprefixer({browsers: ['> 3%', 'last 2 versions', 'IE 10']}))
		.pipe(gulp.dest(destination))
		.pipe(rev())
		.pipe(gulp.dest(destination))
		.pipe(rev.manifest())
		.pipe(revDel({dest: destination, force: true}))
		.pipe(gulp.dest(destination));
}

gulp.task('plugins', function(){
	gulp.src('node_modules/jquery/dist/jquery.min.js')
		.pipe(gulp.dest('./scripts/plugins'));
	gulp.src('node_modules/bootstrap/dist/js/bootstrap.bundle.min.js')
		.pipe(gulp.dest('./scripts/plugins'));
	gulp.src('node_modules/pickadate/lib/picker.js')
		// .pipe(uglify())
		.pipe(gulp.dest('./scripts/plugins'));
	gulp.src('node_modules/pickadate/lib/picker.date.js')
		// .pipe(uglify())
		.pipe(gulp.dest('./scripts/plugins'));
	gulp.src('node_modules/slick-carousel/slick/slick.min.js')
		.pipe(gulp.dest('./scripts/plugins'));
	gulp.src('node_modules/slick-carousel/slick/ajax-loader.gif')
		.pipe(gulp.dest('css'));
	gulp.src('node_modules/slick-carousel/slick/fonts/**.*')
		.pipe(gulp.dest('css/fonts'));
	gulp.src('node_modules/simple-lightbox/dist/simpleLightbox.min.js')
		.pipe(gulp.dest('./scripts/plugins'));
	gulp.src('node_modules/lazysizes/lazysizes.min.js')
		.pipe(gulp.dest('./scripts/plugins'));
	gulp.src('node_modules/instafeed.js/instafeed.min.js')
		.pipe(gulp.dest('./scripts/plugins'));
	gulp.src('node_modules/snazzy-info-window/dist/snazzy-info-window.min.js')
		.pipe(gulp.dest('./scripts/plugins'));
	gulp.src('node_modules/@vimeo/player/dist/player.min.js')
		.pipe(gulp.dest('./scripts/plugins'));

});

gulp.task('scripts', function() {
	return gulp.src('./scripts/index.js')
		// .pipe(uglify())
	  	.pipe(gulp.dest('./dist/'))
		.pipe(rev())
		.pipe(gulp.dest('js'))
		.pipe(rev.manifest())
		.pipe(revDel({
			dest: 'js'
		}))
        .pipe(gulp.dest('js'));
});

gulp.task('watchScripts', ['scripts'], function (done) {
    browserSync.reload();
    done();
});

gulp.task('copy-fonts', function () {
	gulp.src('./node_modules/@fortawesome/fontawesome-free/webfonts/**/*.{eot,svg,ttf,woff,woff2}')
		.pipe(gulp.dest('./webfonts'));
});

gulp.task('themeConfig', function () {
	return gulp.src([currentThemeConfigPath + '/**/**.*', currentThemeConfigPath + '/**.*'  ])
		.pipe(gulp.dest(themePath +'/theme-config'));
});

gulp.task('watchCSS', ['themeConfig'], function () {
	return gulp
		.src('scss/themes/' + currentTheme + '/style.scss')
		.pipe(sassGlob())
		.pipe(sourcemaps.init())
		.pipe(sass({
			outputStyle: 'expanded',
			precision: 4
		}))
		.on('error', notify.onError(function (error) {
			return 'Error: ' + error.message;
		}))
		.pipe(autoprefixer({
			browsers: ['> 3%', 'last 2 versions', 'IE 10']
		}))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('css'))
        .pipe(browserSync.stream());
});


gulp.task('productionCSS', function(){
    return versionFile('scss/themes/' + currentTheme + '/style.scss', 'css', 'style.css');
});


gulp.task('dev', ['copy-fonts', 'plugins', 'themeConfig', 'watchCSS', 'scripts', 'browserSync'], function () {
	gulp.watch('scss/**/*.scss', ['watchCSS']);
	gulp.watch('scripts/index.js', ['watchScripts']);
});

//PRODUCTION TASKS
gulp.task('production', function() {
    runSequence(
		'themeConfig',
		'productionCSS',
        ['copy-fonts',
        'plugins',
		'scripts'
		]
    );
});