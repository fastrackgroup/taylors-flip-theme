var async = async || [],
    themeDir = document.querySelector("body").dataset.themeDir;
(function () { 

    var done = false; 
    var script = document.createElement("script"), 
    head = document.getElementsByTagName("head")[0] || document.documentElement; 

    script.src = themeDir + "/scripts/plugins/jquery.min.js"; 
    script.type = "text/javascript";  
    script.async = true; 
    script.onload = script.onreadystatechange = function() { 

        if (!done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) { 

            done = true; 
            //console.log('jquery main script is ready'); 

            // Process async variable 
            while(async.length) { // there is some syncing to be done 

                var obj = async.shift(); 

                if (obj[0] =="ready") { 
                    $(obj[1]); 
                }else if (obj[0] =="load"){ 
                    $(window).load(obj[1]); 
                } 
            } 

            async = { 
                push: function(param){ 
                    if (param[0] =="ready") { 
                        $(param[1]); 
                    }else if (param[0] =="load"){ 
                        $(window).load(param[1]); 
                    } 
                } 
            }; 

            // End of processing 
            script.onload = script.onreadystatechange = null; 

            if (head && script.parentNode) { 
                head.removeChild(script); 
            } 
        } 
    }; 

    head.insertBefore(script, head.firstChild); 

})(); 

// helper function to load scripts. 
function loadScript(url, callback){ 
    var script = document.createElement("script"); 
    script.type = 'text/javascript';  
    script.async = true; 
    script.src = url; 

    if(callback){
        script.onload = callback;
    }

    var s = document.getElementsByTagName("script")[0]; 

    s.parentNode.insertBefore(script, s); 
}

function hasScript(url) {
    var scripts = document.getElementsByTagName("script");
    for (var i = 0; i < scripts.length; i++) {
        if (scripts[i].href === url) {
            return true;
        }
    }
    return false;
}


// add scripts to the page
//DEPENDENCIES
async.push(["ready",function (){


    //PICKERS
    loadScript(themeDir + "/scripts/plugins/picker.js", function(){
        loadScript(themeDir + "/scripts/plugins/picker.date.js", function(){
            jQuery(function ($) {

                var $bookingForm = $('.booking-form-widget'),
                    $checkinPicker = $('.js-check-in'),
                    $checkoutPicker = $('.js-check-out');

                //Booking Form
                var submitDateFormat = undefined;
                if (hotelData.pmsProvider === 'TheBookingButton') {
                    submitDateFormat = 'yyyy-mm-dd';
                } else if (hotelData.pmsProvider === 'RMS') {
                    submitDateFormat = 'mm/dd/yyyy';
                } else if (hotelData.pmsProvider === 'BookingDirect') {
                    submitDateFormat = 'dd/mm/yyyy';
                }

                var checkOutPicker = $checkoutPicker.pickadate({
                    format: 'd mmm yyyy',
                    min: true,
                    hiddenName: true,
                    formatSubmit: submitDateFormat,
                    onSet: function (context) {

                    }
                });


                var checkInPicker = $checkinPicker.pickadate({
                    format: 'd mmm yyyy',
                    min: true,
                    hiddenName: true,
                    formatSubmit: submitDateFormat,
                    onSet: function (context) {
                        var minDate = context.select + 86400000,
                            checkOutPicker = this.$node.closest('.booking-form').find('.js-check-out');
                        var checkoutSelected = checkOutPicker.pickadate('picker').get('select');

                        if (context.select && (checkoutSelected == null || checkoutSelected.pick < minDate)) {
                            checkOutPicker.pickadate('picker').set('select', minDate);
                            checkOutPicker.pickadate('picker').set('min', new Date(minDate));
                            checkOutPicker.pickadate('picker').open();
                        }

                    }
                });

                var settings = {
                    format: 'd mmm yyyy',
                    min: true,
                    hiddenName: true,
                    formatSubmit: 'd mmm yyyy'
                };
                var customFormDatePickers = document.querySelectorAll('.custom-contact-form-date-pickers');
                customFormDatePickers.forEach(function (datePicker) {
                    jQuery(datePicker).pickadate(settings);
                });

            }); 
        }); 
    });


    //SLIDERS
    loadScript(themeDir + "/scripts/plugins/slick.min.js", function(){
        jQuery(function ($) {
            var $window = $(window),
            $body = $('body');
    
            var $heroSLider = $('.hero-slider'),
                $heroSLides = $('.hero-slide'),
                $cardSlider = $('[data-slider]'),
                $whatsOnSlider = $('.whats-on-slider'),
                $whatsOnSlides = $whatsOnSlider.find('.card'),
                theme = $body.data('theme'),
                whatsOnSlides = theme ==='gls' ? 3 : 2;

            //init hero slider
            if($heroSLides.length > 1){
                $heroSLider.slick({
                    autoplay: true,
                    autoplaySpeed: 5000,
                    infinite: true,
                    fade: true,
                    cssEase: 'linear',
                    lazyLoad: 'ondemand',
                    responsive: true,
                    arrows: theme === 'gls' ? false : true,
                    prevArrow:"<button type='button' class='slick-prev pull-left'><i class='fas fa-arrow-left' aria-hidden='true'></i></button>",
                    nextArrow:"<button type='button' class='slick-next pull-right'><i class='fas fa-arrow-right' aria-hidden='true'></i></button>",
                    dots: theme === 'bounce' ? false : true
                });
            }

            //init whats-on-slider slider
            if($whatsOnSlides.length > whatsOnSlides){
                $whatsOnSlider.slick({
                    autoplay: true,
                    autoplaySpeed: 5000,
                    infinite: true,
                    lazyLoad: 'ondemand',
                    rows: 0,
                    slidesToShow: whatsOnSlides,
                    slidesToScroll: whatsOnSlides,
                    responsive: [
                        {
                            breakpoint: 992,
                            settings:{
                                slidesToShow: 2,
                                slidesToScroll: 2
                            }
                        },
                        {
                            breakpoint: 768,
                            settings:{
                                slidesToShow: 1,
                                slidesToScroll: 1
                            }
                        }
                    ],
                    arrows: true,
                    prevArrow:"<button type='button' class='slick-prev pull-left'><i class='fas fa-arrow-left' aria-hidden='true'></i></button>",
                    nextArrow:"<button type='button' class='slick-next pull-right'><i class='fas fa-arrow-right' aria-hidden='true'></i></button>"
                });
            }

            //init slider on each room card
            if($cardSlider.length){

                $cardSlider.each( function(){

                    if($(this).children().length < 2 ) 
                        return true;

                    $(this).slick({
                        lazyLoad: 'ondemand',
                        responsive: true,
                        infinite: true,
                        arrows: true,
                        dots:  theme === 'bounce' ? false : true,
                        prevArrow:"<button type='button' class='slick-prev pull-left'><i class='fas fa-arrow-left' aria-hidden='true'></i></button>",
                        nextArrow:"<button type='button' class='slick-next pull-right'><i class='fas fa-arrow-right' aria-hidden='true'></i></button>"
                    });
                })
            }
        });
    });


    //LIGHTBOX
    if(document.querySelector('.js-gallery-link')){
        loadScript(themeDir + "/scripts/plugins/simpleLightbox.min.js", function(){
            jQuery(function ($) {
                var $galleryLink = $('.js-gallery-link');
                $galleryLink.simpleLightbox();
            });
        }); 
    }


    // BOOTSTRAP
    var loadBootstrap = document.querySelector('.bootstrap-dropdown') ||
        document.querySelector('.modal');
    if (loadBootstrap) {
        loadScript(themeDir + "/scripts/plugins/bootstrap.bundle.min.js");
    }


    // Vimeo
    loadScript(themeDir + "/scripts/plugins/player.min.js", function() {
        const vimeoHero = document.querySelector('.vimeo-hero');
        if (vimeoHero) {
            vimeoHero && Object.create(VimeoHero).init(vimeoHero);
            document.querySelectorAll('.vimeo-play').forEach((vimeoPlayButton) => {
                Object.create(VimeoVideo.init(vimeoPlayButton));
            });
        }
    });


    // MAPS
    var thingsToDoMap = document.querySelector('.things-todo-map');
    if(thingsToDoMap){
        var mapKey = thingsToDoMap.dataset.apiKey;
        loadScript("https://maps.googleapis.com/maps/api/js?key=" + mapKey, function(){
            loadScript(themeDir + "/scripts/plugins/snazzy-info-window.min.js", function(){
                if (hotelData.hasThingsToDoMap && hotelData.placeCategories && hotelData.placeCategories.length) {

                    jQuery(function ($) {
                        $.fn.isInViewport = function() {
                            var elementTop = $(this).offset().top,
                                viewportTop = $(window).scrollTop(),
                                viewport = viewportTop + window.innerHeight;
                    
                            return viewport > elementTop;
                        };

                        if($('.things-todo-map').isInViewport()){
                            placesMap.init();
                        }
                        else{
                            $('.things-todo-map').on('lazybeforeunveil', function(e){
                                placesMap.init();
                            });
                        }
                    });
                }
            });
        }); 
    }

}]
); 

// User interactive UI elements
async.push(["ready",function (){ 

    jQuery(function ($) {
        var $window = $(window),
            $body = $('body'),
            $header = $('.header'),
            $menu = $('#menu'),
            $toggleHeader = $('.js-toggle-menu'),
            $toggleBooking = $('.js-toggle-booking'),
            $bookingForm = $('.booking-form-widget'),
            $bookingFormContainer = $('.booking-form-container'),
            $navigationBookNowBtn = $('.book-now-link'),
            theme = $body.data('theme');

        if(theme === 'ballina'){
            var $bookingForm = $('.booking-form-wrap'),
                formFromTop = $bookingForm.offset().top,
                headerHeight = $header.innerHeight() ;
        }

        $window.on('scroll load', function(){ 

            var scrollFromTop = $window.scrollTop();

            if (scrollFromTop >= 10)
                $header.addClass('is-sticky');
            else
                $header.removeClass('is-sticky');

            if(theme === 'ballina'){
                if (scrollFromTop >= formFromTop){
                    $bookingForm
                        .addClass('is-sticky')
                        .css('top', headerHeight);
                }
                else{
                    $bookingForm
                        .removeClass('is-sticky')
                        .css('top', 'auto');
                }   
            }
        });

        if ($navigationBookNowBtn) {
            $navigationBookNowBtn.on('click', function (event) {
                event.preventDefault();
                $bookingFormContainer.toggleClass('show');
            });
        }

        $toggleHeader.on('click', function(){
            $(this).toggleClass('show');
            $menu.toggleClass('show');
        });

        $toggleBooking.on('click', function(){
            $(this).toggleClass('show');
            $bookingForm.toggleClass('show');
            $bookingFormContainer.toggleClass('show');
            $toggleHeader.removeClass('show');
            $menu.removeClass('show');
        });

        // Smooth scrolling
        $(document).on('click', 'a[href^="#"]', function (event) {
            event.preventDefault();

            $('html, body').animate({
                scrollTop: $($.attr(this, 'href')).offset().top
            }, 500);
        });

        //TODO: async on the plugin
        if (window.hotelData.instafeed) {
            var feed = new Instafeed({
                get: 'user',
                userId: window.hotelData.instafeed.userId,
                clientId: window.hotelData.instafeed.clientId,
                accessToken: window.hotelData.instafeed.accessToken,
                resolution: 'standard_resolution',
                template: '<div class="col-6 col-md-3">' +
                                '<a href="{{link}}" target="_blank">' +
                                    '<div class="tile-squared">' +
                                        '<img src="{{image}}" class="absolute-cover-image">' +
                                    '</div>' +
                                '</a>' +
                            '</div>',
                limit: 7
            });
    
            $('.instagram-feeds').on('lazybeforeunveil', function(e){
                feed.run();
            });
        }
    });

}]);


// THINGS TO DO

const VimeoHero = {

    element: null,

    init: function (element) {
        this.element = element;
        const options = {
            id: this.element.dataset.videoId,
            background: true,
            height: this.getHeight(),
        };
        new Vimeo.Player(element.id, options);
    },

    getHeight: function () {
        const ratio = this.element.dataset.ratio ? this.element.dataset.ratio.split(":") : [16, 9];
        const quotient = parseInt(ratio[1]) / parseInt(ratio[0]);
        const minHeight = this.convertRemToPixels(40);
        const width = window.innerWidth < minHeight ? minHeight : window.innerWidth;
        const height = window.innerHeight;
        return (width * quotient > height) ? width * quotient : height;
    },

    convertRemToPixels: function (rem) {
        return rem * parseFloat(getComputedStyle(document.documentElement).fontSize);
    }

};

const VimeoVideo = {

    init: function (vimeoPlayButton) {
        vimeoPlayButton.addEventListener('click', function (event) {
            event.preventDefault();
            const video = document.querySelector(this.dataset.video);
            const vimeoPlayer = new Vimeo.Player(video.id);
            vimeoPlayer.loadVideo({
                id: video.dataset.vimeoId,
                controls: true,
                autoplay: true,
            });
            this.remove();
        });
    },

};

var placesMap = {
    map: null,
    mapOptions: {
        center: null,
        disableDefaultUI: false,
        streetViewControl: false,
        zoomControl: true,
        fullscreenControl: false,
        mapTypeControl: false
    },
    markers: {},
    clearMarkers: true,

    init: function () {
        this.bounds = new google.maps.LatLngBounds();
        if (hotelData.thingsToDoMapStyle) {
            this.mapOptions.styles = hotelData.thingsToDoMapStyle;
        }
        this.mapOptions.center = new google.maps.LatLng(hotelData.location.latitude, hotelData.location.longitude);
        this.map = new google.maps.Map(document.getElementById('things-todo-map'), this.mapOptions);
        this.bounds.extend(this.mapOptions.center);
        this.createMarker(hotelData.location, hotelData.location.mapPin);
        this.initMarkers();
        this.setEventListenersOnToggleButtons();
        this.map.fitBounds(this.bounds, {top:400, bottom:0});
        this.map.setZoom(this.map.getZoom() - 1);
        if (this.map.getZoom() > 15) {
            this.map.setZoom(15);
        }
    },

    initMarkers: function () {
        for (var i = 0; i < hotelData.placeCategories.length; i++) {
            this.markers[hotelData.placeCategories[i].key] = [];
            for (var j = 0; j < hotelData.placeCategories[i].places.length; j++) {
                this.markers[hotelData.placeCategories[i].key].push(
                    this.createMarker(hotelData.placeCategories[i].places[j], hotelData.placeCategories[i].icon));
            }
        }
    },

    createMarker: function (place, icon) {
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(place.latitude, place.longitude),
            map: this.map,
            icon: {
                size: new google.maps.Size(44, 66),
                scaledSize: new google.maps.Size(44, 66),
                url: icon
            }
        });
        this.bounds.extend(marker.position);
        this.addInfoWindow(marker, place);
        return marker;
    },

    addInfoWindow: function (marker, place) {
        var content = '<h4>' + place.name + '</h4><ul>';
        if (place.address) {
            content += '<li class="address"> <i class="fas fa-map-marker-alt"></i>' + place.address + '</li>';
        }
        if (place.phone) {
            content += '<li class="phone"><i class="fas fa-mobile-alt"></i>' + place.phone + '</li>';
        }
        content += '</ul>';
        new SnazzyInfoWindow({
            map: this.map,
            marker: marker,
            content: content,
            closeWhenOthersOpen: true
        });
    },

    setEventListenersOnToggleButtons: function () {
        var toggleButtons = document.querySelectorAll('.js-place-map-toggle-btn');
        var allButton = document.getElementById('show-all-todo');

        for (var i = 0; i < toggleButtons.length; i++) {
            toggleButtons[i].addEventListener('click', function (event) {
                
                var isAll = event.target.dataset.placeCategory === 'all';
                var hasAllBtnActiveClass = allButton.classList.contains('active');
                
                if (isAll) {
                    for (var i = 0; i < toggleButtons.length; i++) {
                        toggleButtons[i].classList.remove('active');
                    }

                    if (hasAllBtnActiveClass) {
                        allButton.classList.remove('active');
                        this.toggleMarkersOfType(event.target.dataset.placeCategory, true);
                    } else {
                        allButton.classList.add('active');
                        this.toggleMarkersOfType(event.target.dataset.placeCategory, false);
                    }
                } else {
                    allButton.classList.remove('active');
                    event.target.classList.toggle('active');
                    this.toggleMarkersOfType(event.target.dataset.placeCategory, hasAllBtnActiveClass);
                }

            }.bind(this), false);
        }
    },

    clearAllMarkers: function(){
        for (var i = 0; i < hotelData.placeCategories.length; i++) {
            var categoryKey = hotelData.placeCategories[i].key;
            for (var j = 0; j < this.markers[categoryKey].length; j++) {
                this.markers[categoryKey][j].setVisible(this.clearMarkers);
            }
        }
    },

    toggleMarkersOfType: function (category, clearAll) {
        this.clearMarkers = !clearAll;
        if (category==='all') {
            this.clearAllMarkers();
        } else {
            if (clearAll) {
                this.clearAllMarkers();
            }
            for (var i = 0; i < this.markers[category].length; i++) {
                this.markers[category][i].setVisible(!this.markers[category][i].getVisible());
            }
        }
    }
};

if (document.querySelector('.g-recaptcha')) {
    if (!hasScript('https://www.google.com/recaptcha/api.js')) {
        loadScript('https://www.google.com/recaptcha/api.js', false);
    }
}




const telLinks = document.querySelectorAll('a[href^="tel:"');
telLinks.forEach(telLink => {
	let link = telLink.href.replaceAll(' ', '');
	if (!link.includes('+64')) {
		link = link.replace('tel:', 'tel:+64');
	}
	telLink.href = link;
});




