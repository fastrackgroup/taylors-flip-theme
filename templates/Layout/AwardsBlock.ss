
<section class="awards">
    <div class="container">

        <% if $ShowTitleAs != 'Do not show' %>
            <div class="row">
                <div class="col heading-alignment">
                    <% if $SubTitle %><div class="sub-title">$SubTitle</div><% end_if %>
                    <$ShowTitleAs>$Title</$ShowTitleAs>
                </div>
            </div>
        <% end_if %>


        <div class="row">
            <% loop $Awards.Sort('SortOrder') %>
                <div class="col-lg-4 mb-gutter">
                    <div class="awards-card">
                        <div class="awards-card-media">
                            $Image.getImageTag('auto=format&h=600&fit=max&q=80', 'card-img-top lazyload img-ie-fix', 'true')
                        </div>
                        <div class="awards-card-description">
                            <h3>$Title</h3>
                            <% if $Year && $AwardedBy %>
                                <p><strong>$Year</strong> - $AwardedBy</p>
                            <% else_if $Year %>
                                <p><strong>$Year</strong></p>
                            <% else_if $AwardedBy %>
                                <p><strong>$AwardedBy</strong></p>
                            <% end_if %>
                            $Body.RAW
                        </div>
                    </div>
                </div>
            <% end_loop %>
        </div>
    
    </div>

</section>

